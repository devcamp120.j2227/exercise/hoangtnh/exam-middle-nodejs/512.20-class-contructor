//import thư viện express JS
const express = require("express");

//khởi tạo app 
const app = express() ;

//import thư viện path
const path = require ("path");
//Khai báo cổng chạy
const port = 8000;

//Khai báo router
const browserRouter = require ("./app/routes/browserRouter");

//app sử dụng router
app.use("/",browserRouter);

//call api chạy browers
app.get("/", (request, response) => {
    response.sendFile(path.join(__dirname + "/view/projectJs.html"));
});

//Chạy app trên cổng đã khai báo
app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})
